---
title: Bienvenue
description: Ce site comporte les cours de Physique chimie pour le niveau Seconde.
---

# Bienvenue
 
Ce site présente les cours de Physique chimie pour le niveau Seconde. Il a été conçu pour les élèves du lycée Bossuet à Condom.

Si vous êtes élèves, ce site vous permettra : de mettre votre classeur à jour, rattraper les cours que vous auriez manqués, vous exercer, avoir accès aux contenus multimédias diffusées en classe, consulté les sujets et les corrigés des DM et même pour les plus motivés à avoir accès à des contenues suplémentaires …


<center>
    <a href="./assets_extra/images/qr-code2_a4.pdf">
        <img src="./assets_extra/images/qr-code3.svg" alt="Qr code du site" style="width:300px;"/>
    </a>
</center>