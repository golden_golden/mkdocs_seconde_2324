﻿---
title : "Activité 1"
correctionvisible : true
---


<div class="lien_pdf_sujet">
  <a  href="../pdf/activites/2_PC_Chapitre4_Activite1.pdf">Sujet.pdf </a>
  

  <a href="../pdf/activites/Corrections/2_PC_Chapitre4_Activite1_Correction.pdf" class="correction">Correction.pdf</a>
</div>



# Activité 1 :

<center>
<iframe src="../pdf/activites/2_PC_Chapitre4_Activite1.pdf" height="1400" width="1200"></iframe>
</center>




<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

