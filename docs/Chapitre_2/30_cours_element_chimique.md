---
title : "Cours"
---


<div class="lien_pdf_sujet">
  <a  href="../pdf/cours/2_PC_Chapitre 2_Cours2.pdf">Cours.pdf </a>
</div>

# Chapitre 2 : 

<center>
    <img src="../pdf/cours/2_PC_Chapitre 2_Cours2_p1.webp" width="1200" />
</center>

!!! note "Liens vidéo"
    - [Paul olivier_composition d'un atome](https://www.youtube.com/watch?v=3okQp-SHmaw)
    - [Romain Bourdel-Chapuzot](https://www.youtube.com/watch?v=dqHmMU3hkoM)
    - [Les Bons Profs - atomes et ions](https://www.youtube.com/watch?v=9fiCedUe1Sk)
    - [Soutien SPC - La constitution de l'atome en 3min](https://www.youtube.com/watch?v=jbHGfAjvcmM)


    - [Paul olivier_Calculer la masse d'un Atome](https://www.youtube.com/watch?v=5cKzrWGsXRg)
    - [Paul olivier_Calculer la masse d'un Atome](https://www.youtube.com/watch?v=l7JwWMnjY8g)
    - [Les génies des sciences_Comment calculer la masse d'un atome et de son noyau en 5 minutes](https://www.youtube.com/watch?v=CeFoYSEORyE)
