---
title : "Exercices"
correctionvisible : True

---

<div class="lien_pdf_sujet">
  <a  href="../pdf/exercices/2_PC_Chapitre2_DM2.pdf">Sujet.pdf </a>
  
  <a href="../pdf/exercices/Correction/2_PC_Chapitre2_DM2_correction.pdf" class="correction">Correction.pdf</a>
</div>

<br>

# Exercices : Le noyau de l'atome


## Représentation symbolique des atomes
- **Exercice 1 :** 33 <a href="../images/exercices/manuel/p69.webp">p69</a>
- **Exercice 2 :** 51 <a href="../images/exercices/manuel/p72.webp">p72</a>-<a href="../images/exercices/manuel/p73.webp">p73</a>


## Masse des atomes
- **Exercice 3 :** 42 <a href="../images/exercices/manuel/p70.webp">p70</a>
- **Exercice 4 :** 48 <a href="../images/exercices/manuel/p72.webp">p72</a>
- **Exercice 5 :** 44 <a href="../images/exercices/manuel/p71.webp">p71</a>
- **Exercice 6 :** 49 <a href="../images/exercices/manuel/p72.webp">p72</a>

- **Exercice 7 :** 55 <a href="../images/exercices/manuel/p74.webp">p74</a> *(Masse volumique)*




## Exercice d'entraînement *(avec correction)*
- **Exercice 8 :** 43 <a href="../images/exercices/manuel/p71.webp">p71</a>



[qcm ludique](https://www.youtube.com/watch?v=5iO7rpPYizA)



<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

