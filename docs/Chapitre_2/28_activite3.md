﻿---
title : "Activité 3"
correctionvisible : True
---


<div class="lien_pdf_sujet">
  <a  href="../pdf/activites/2_PC_Chapitre2_Activite3.pdf">Sujet.pdf </a>
  

  <a href="../pdf/activites/Corrections/2_PC_Chapitre2_Activite3_Correction.pdf" class="correction">Correction.pdf</a>
</div>



# Activité 3 : La représentation sympolique de l'atome


<center>
    <img src="../pdf/activites/2_PC_Chapitre2_Activite3.webp" width="1200" />
</center>


<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

