---
title : "Exercices"
correctionvisible : True
---

<div class="lien_pdf_sujet">
  <a  href="../pdf/exercices/2_PC_Chapitre2_DM1.pdf">Sujet.pdf </a>
  

  <a href="../pdf/exercices/Correction/2_PC_Chapitre2_DM1_correction.pdf" class="correction">Correction.pdf</a>
</div>

<br>

# Exercices : Le noyau de l'atome


## Dimensions de l'atome


- **Exercice 1 :** 30 <a href="../images/exercices/manuel/p69.webp">p69</a>
- **Exercice 2 :** 41 <a href="../images/exercices/manuel/p70.webp">p70</a>
- **Exercice 3 :** 38 <a href="../images/exercices/manuel/p70.webp">p70</a>
- **Exercice 4 :** 54 <a href="../images/exercices/manuel/p73.webp">p73</a>


<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

