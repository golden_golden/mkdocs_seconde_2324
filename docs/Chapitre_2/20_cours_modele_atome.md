---
title : "Cours"
---


<div class="lien_pdf_sujet">
  <a  href="../pdf/cours/2_PC_Chapitre 2_Cours1.pdf">Cours.pdf </a>
</div>


# Chapitre 2 : 

<center>
    <img src="../pdf/cours/2_PC_Chapitre 2_Cours1_p1.webp" width="1200" />
    <img src="../pdf/cours/2_PC_Chapitre 2_Cours1_p2.webp" width="1200" />
</center>



??? note "Liens vidéo"
    - [Romain Bourdel-Chapuzot](https://www.youtube.com/watch?v=dqHmMU3hkoM)
    - [Yvan Monka_Écrire un nombre avec des puissances de 10](https://www.youtube.com/watch?v=D5Fe9Fv6CqQ)
    - [Yvan Monka_Effectuer des calculs avec des puissances de 10](https://www.youtube.com/watch?v=EL4dBiBbL-U)
    - [Paul Olivier_Taille d'un atome et structure lacunaire](https://www.youtube.com/watch?v=qKobuRfsS8E)