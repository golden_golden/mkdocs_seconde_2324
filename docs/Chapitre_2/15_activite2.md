﻿---
title : "Activité 2"
correctionvisible : True
---


<div class="lien_pdf_sujet">
  <a  href="../pdf/activites/2_PC_Chapitre2_Activite2.pdf">Sujet.pdf </a>
  

  <a href="../pdf/activites/Corrections/2_PC_Chapitre2_Activite2_Correction.pdf" class="correction">Correction.pdf</a>
</div>



# Exercice : Dimensions de l'atome


1. Sachant que l’atome a un rayon de l’ordre de $10^{-10} m$ et que pour le noyau c’est de l’ordre de $10^{-15} m$, quel serait le rayon de l’atome si le noyau était représenté par une sphère de $10 cm$ de rayon ?
<br><br>
2. Est-il possible de représenter un atome et son noyau à l’échelle sur une copie ? Justifier.


<br><br><br>

??? note "Liens vidéo"

    - [Le plus petit film au monde !](https://www.youtube.com/watch?v=oSCX78-8-q0)
    - [Microscope à effet tunel](https://www.youtube.com/watch?v=NEsbREz-BBU)

    - [zoom sur la matière](https://www.youtube.com/shorts/yYckT0jrMt4)
    
    - [vidéo cea](https://www.cea.fr/comprendre/enseignants/Pages/ressources-pedagogiques/videos/physique-chimie/definition-matiere.aspx)


<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

