€ Justifier que la réaction de fusion dans le Soleil décrite dans le doc. 1 est de
nature nucléaire.

2 ) Indiquer le point commun des noyaux utilisés dans le réacteur d’ITER (doc. 2)
et de ceux impliqués dans la réaction nucléaire du Soleil.

Œ À Paide du doc. 2, écrire l'équation de la réaction nucléaire se produisant
dans le réacteur d’ITER.

@ Calculer la masse d’hydrogène consommée dans le Soleil nécessaire pour
libérer une quantité d’énergie égale à celle d’ITER (doc. 1 et doc.2).

@ Comparer les réactions dans le Soleil et dans ITER. Justifier le terme de fusion
thermonucléaire.

e 0 Ct0 10 000100 V0 e T0 e e e e le0Ve" 0707010100 u7000 010000 00160 00 00 e 00-0 00 0 0 0 0 0 0 RIR

@ Expliquer l’origine de l’énergie libérée par le Soleil et par ITER. ;
