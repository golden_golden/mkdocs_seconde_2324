Lise Meitner et la découverte
de la fission nucléaire

La physicienne autrichienne naturalisée suédoise L. Meitner est
connue pour ses travaux de recherche en physique nucléaire*
et son rôle dans la découverte de la fission nucléaire.

P Qu'est-ce qu’une fission nucléaire ?

