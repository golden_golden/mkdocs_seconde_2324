- [https://www.kartable.fr/ressources/physique-chimie/cours/les-transformations-nucleaires/49895](https://www.kartable.fr/ressources/physique-chimie/cours/les-transformations-nucleaires/49895)
- [https://www.lesbonsprofs.com/cours/transformation-nucleaire/](https://www.lesbonsprofs.com/cours/transformation-nucleaire/)

vidéo :
- [ceb la vallée de la stabilité (début)](https://www.youtube.com/watch?v=VZHpAwSGYZE)
- [https://www.youtube.com/watch?v=YZMy7wOP628](https://www.youtube.com/watch?v=YZMy7wOP628)
- [https://www.youtube.com/watch?v=bHylCliG89Y](https://www.youtube.com/watch?v=bHylCliG89Y)
- [https://www.youtube.com/watch?v=Ke4n5QvCq0A](https://www.youtube.com/watch?v=Ke4n5QvCq0A)
- [https://www.youtube.com/watch?v=m7MsI87s_FU](https://www.youtube.com/watch?v=m7MsI87s_FU)
- [https://www.youtube.com/watch?v=rAKJ7HKUwGg](https://www.youtube.com/watch?v=rAKJ7HKUwGg)
- [https://www.youtube.com/watch?v=2dKaCpZR9Qg](https://www.youtube.com/watch?v=2dKaCpZR9Qg)

- [https://www.youtube.com/watch?v=H3QVkmEh-Fw](https://www.youtube.com/watch?v=H3QVkmEh-Fw)
- [https://www.youtube.com/watch?v=pbwrGH9alCg](https://www.youtube.com/watch?v=pbwrGH9alCg)