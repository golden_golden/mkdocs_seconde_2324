# Le programme scolaire

<center>
<iframe src="../pdf/programme.pdf" height="800" width="1200"></iframe>
</center>


!!! note "Ressources"
    - [Éduscol_Programme pour le niveau seconde générale](https://eduscol.education.fr/document/22663/download)
    - [Éduscol_Ressources pour la physique chimie voie générale](https://eduscol.education.fr/1648/programmes-et-ressources-en-physique-chimie-voie-gt)