# Compétences travaillées dans le cadre de la démarche scientifiques


<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Compétences</strong></center></td>
            <td><center><strong>Quelques exemples de capacités associée</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><strong>S’approprier</strong></td>
            <td>
                <li>Énoncer une problématique.</li>
                <li>Rechercher et organiser l’information en lien avec la problématique étudiée.</li>
                <li>Représenter la situation par un schéma.</li>
            </td>
        </tr>
        <tr>
            <td><strong>Analyser/ Raisonner</strong></td>
            <td>
                <li>Formuler des hypothèses.</li>
                <li>Proposer une stratégie de résolution.</li>
                <li>Planifier des tâches.</li>
                <li>Évaluer des ordres de grandeur.</li>
                <li>Choisir un modèle ou des lois pertinentes.</li>
                <li>Choisir, élaborer, justifier un protocole.</li>
                <li>Faire des prévisions à l’aide d’un modèle.</li>
                <li>Procéder à des analogies.</li>
            </td>
        </tr>
        <tr>
            <td><strong>Réaliser</strong></td>
            <td>
                <li>Mettre en œuvre les étapes d’une démarche.</li>
                <li>Utiliser un modèle.</li>
                <li>Effectuer des procédures courantes (calculs, représentations, collectes de données, etc.).</li>
                <li>Mettre en œuvre un protocole expérimental en respectant les règles de sécurité.</li>
            </td>
        </tr>
        <tr>
            <td><strong>Valider</strong></td>
            <td>
                <li>Faire preuve d’esprit critique, procéder à des tests de vraisemblance.</li>
                <li>Identifier des sources d’erreur, estimer une incertitude, comparer à une valeur de référence.</li>
                <li>Confronter un modèle à des résultats expérimentaux.</li>
                <li>Proposer d’éventuelles améliorations de la démarche ou du modèle.</li>
            </td>
        </tr>
        <tr>
            <td><strong>Communiquer</strong></td>
            <td>
                À l’écrit comme à l’oral :
                <li>Présenter une démarche de manière argumentée, synthétique et cohérente ;</li>
                <li>Utiliser un vocabulaire adapté et choisir des modes de représentation appropriés ;</li>
                <li>Échanger entre pairs.</li>
            </td>
        </tr>
    </tbody>
</table>
</center>
