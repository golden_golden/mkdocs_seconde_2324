
# Progression et répartition du programme par chapitre


## 1. Progression 

Le programme est bâti autour de quatre thèmes.

<center>
<table>
    <tr>
      <td>1</td>
      <td>Constitution et transformations de la matière</td>
      <td>lg1</td>
    </tr>
      <tr>
      <td>2</td>
      <td>L’énergie : conversions et transferts</td>
      <td>lg2</td>
    </tr>
      <tr>
      <td>3</td>
      <td>Mouvement et interactions</td>
      <td>lg3</td>
    </tr>
      <tr>
      <td>4</td>
      <td>Ondes et signaux</td>
      <td>lg4</td>
    </tr>
</table>
</center>


<br><br>

Dans l’objectif d’offrir une meilleure répartition des notions de physique et de chimie tout au long de l’année, ces thématiques ne seront pas traitées en bloc. Ainsi, la progression retenue est :

<center>
<table>
    <thead>
        <tr>
            <td><center>Semaine</center></td>
            <td><center>Thème</center></td>
            <td><center><strong>Chapitre</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>lg1</td>
            <td>Atomes, ions, molécules</td>
        </tr>
        <tr>
            <td></td>
            <td>lg</td>
            <td>Propagation de la lumière et spectres</td>
        </tr>
        <tr>
            <td></td>
            <td>lg</td>
            <td>Transformation physique</td>
        </tr>
        <tr>
            <td></td>
            <td>lg</td>
            <td>Étude du mouvement</td>
        </tr>
        <tr>
            <td></td>
            <td>lg</td>
            <td>Quantité de matière, préparation de solution et dosage</td>
        </tr>
        <tr>
            <td></td>
            <td>lg</td>
            <td>Forces et principe d’inertie</td>
        </tr>
        <tr>
            <td></td>
            <td>lg</td>
            <td>La transformation chimique</td>
        </tr>
        <tr>
            <td></td>
            <td>lg</td>
            <td>Les ondes sonores</td>
        </tr>
        <tr>
            <td></td>
            <td>lg</td>
            <td>Introduction à l’optique</td>
        </tr>
        <tr>
            <td></td>
            <td>lg</td>
            <td>Électricité</td>
        </tr>
        <tr>
            <td></td>
            <td>lg</td>
            <td>La transformation nucléaire</td>
        </tr>
    </tbody>
</table>
</center>






## 2. Répartition du programme par chapitre 


### Atomes, ions, molécules
#### Entités chimiques
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Entités chimiques : molécules, atomes, ions.<br>
            </td>
            <td>
                Utiliser le terme adapté parmi molécule, atome, anion et cation pour qualifier une entité chimique à partir d’une formule chimique donnée.
            </td>
        </tr>
        <tr>
            <td>
                Du macroscopique au microscopique, de l’espèce chimique à l’entité.<br>
                Espèces moléculaires, espèces ioniques, électroneutralité de la
                matière au niveau macroscopique.<br>
            </td>
            <td>
                Définir une espèce chimique comme une collection d’un nombre très élevé d’entités identiques.<br>
                Exploiter l’électroneutralité de la matière pour associer des espèces ioniques et citer des formules de composés ioniques.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

<br>

#### Le modèle atomique
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Le noyau de l’atome, siège de sa masse et de son identité.<br>
                Numéro atomique, nombre de masse, écriture conventionnelle :
                𝑍 𝐴𝑋 ou 𝐴𝑋.<br>
                Élément chimique.<br>
                Masse et charge électrique d’un électron, d’un proton et d’un neutron, charge électrique élémentaire, neutralité de l’atome<br>
            </td>
            <td>
                Citer l’ordre de grandeur de la valeur de la taille d’un atome.<br>
                Comparer la taille et la masse d’un atome et de son noyau.<br>
                Établir l’écriture conventionnelle d’un noyau à partir de sa composition et inversement.<br><br>

                <strong>Capacité mathématique :</strong> effectuer le quotient de deux grandeurs pour les comparer. Utiliser les opérations sur les puissances de 10. Exprimer les valeurs des grandeurs en écriture scientifique.
            </td>
        </tr>
    </tbody>
</table>
</center>
a reprendre notation

<br>

### La transformation nucléaire
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Isotopes.<br>
                Écriture symbolique d’une réaction nucléaire. Aspects énergétiques des transformations nucléaires : Soleil, centrales nucléaires.<br>
            </td>
            <td>
                Identifier des isotopes.<br>
                Relier l’énergie convertie dans le Soleil et dans une centrale nucléaire à des réactions nucléaires.<br>
                Identifier la nature physique, chimique ou nucléaire d’une transformation à partir de sa description ou d’une écriture symbolique modélisant la transformation.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

<br>


#### La structure électronique et tableau périodique


- Structure électronique
- Le tableau périodique des éléments



<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <strong>Le cortège électronique de l’atome définit ses propriétés chimiques.</strong><br>
                Configuration électronique (1s, 2s, 2p, 3s, 3p) d’un atome à l’état fondamental et position dans le tableau périodique (blocs s et p).<br>
                Électrons de valence.<br>
                Familles chimiques.<br>
            </td>
            <td>
                Déterminer la position de l’élément dans le tableau périodique à partir de la donnée de la configuration électronique de l’atome à l’état fondamental.<br><br>

                Déterminer les électrons de valence d’un atome (Z ⩽ 18) à partir de sa configuration électronique à l’état fondamental ou de sa position dans le tableau périodique.<br>
                Associer la notion de famille chimique à l’existence de propriétés communes et identifier la famille des gaz nobles.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>





#### Vers des éléments chimique plus stables

- Formation des ions
- Le modèle de Lewis
- La liaison chimique
- Énergie de liaison



<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <strong>Vers des entités plus stables chimiquement.</strong><br>
                Stabilité chimique des gaz nobles et configurations électroniques associées.
                Ions monoatomiques.<br><br>

                Molécules.<br>
                Modèle de Lewis de la liaison de valence, schéma de Lewis, doublets liants et non-liants.<br> Approche de l’énergie de liaison<br>
                </td>
            <td>
                Établir le lien entre stabilité chimique et configuration électronique de valence d’un gaz noble.<br>
                Déterminer la charge électrique d’ions monoatomiques courants à partir du tableau périodique.<br>
                Nommer les ions : H+, Na+, K+, Ca2+, Mg2+, Cl-, F- ; écrire leur formule à partir de leur nom.<br><br>

                Décrire et exploiter le schéma de Lewis d’une molécule pour justifier la stabilisation de cette entité, en référence aux gaz nobles, par rapport aux atomes isolés (Z ⩽ 18).<br>
                Associer qualitativement l’énergie d’une liaison entre deux atomes à l’énergie nécessaire pour rompre cette liaison.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

a reprendre notation






<br>

### Propagation de la lumière et spectres
#### Propagation de la lumière
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Propagation rectiligne de la lumière.<br>
                Vitesse de propagation de la lumière dans le vide ou dans l’air.<br>
            </td>
            <td>
                Citer la valeur de la vitesse de la lumière dans le vide ou dans l’air et la comparer à d’autres valeurs de vitesses couramment rencontrées.
            </td>
        </tr>
    </tbody>
</table>
</center>

#### Lumière et spectres
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Dispersion de la lumière blanche par un prisme ou un réseau.<br>
            </td>
            <td>
                Produire et exploiter des spectres d’émission obtenus à l’aide d’un système dispersif et d’un analyseur de spectre.<br>
            </td>
        </tr>
        <tr>
            <td>
                Lumière blanche, lumière colorée.<br>
                Spectres d’émission : spectres continus d’origine thermique, spectres de raies.<br>
                Longueur d’onde dans le vide ou dans l’air.<br>
            </td>
            <td>
                Caractériser le spectre du rayonnement émis par un corps chaud.<br>
                Caractériser un rayonnement monochromatique par sa longueur d’onde dans le vide ou dans l’air.<br>
                Exploiter un spectre de raies.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

<br>

### Transformation physique
####  Les états physiques (rappels)
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Écriture symbolique d’un changement d’état.<br>

            </td>
            <td>
                Modélisation microscopique d’un changement d’état.
                Citer des exemples de changements d’état physique de la vie courante et dans l’environnement.<br>
                Établir l’écriture d’une équation pour un changement d’état. Distinguer fusion et dissolution.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

#### Changements d'états et énergie
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Transformations physiques endothermiques et exothermiques.<br>
                Énergie de changement d’état et applications.<br>
            </td>
            <td>
                Identifier le sens du transfert thermique lors d’un changement d’état et le relier au terme exothermique ou endothermique.<br>
                Exploiter la relation entre l’énergie transférée lors d’un changement d’état et l’énergie massique de changement d’état de l’espèce.<br>
                Relier l’énergie échangée à la masse de l’espèce qui change d’état.
            </td>
        </tr>
    </tbody>
</table>
</center>

<br>

### Étude du mouvement

#### Décrire le mouvement

**Système, référentiel**

<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Système.<br>
                Échelles caractéristiques d’un système.<br>
                Référentiel et relativité du mouvement.<br>
            </td>
            <td>
                Identifier les échelles temporelles et spatiales pertinentes de description d’un mouvement.<br>
                Choisir un référentiel pour décrire le mouvement d’un système.<br>
                Expliquer, dans le cas de la translation, l’influence du choix du référentiel sur la description du mouvement d’un système.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

**Décrire le mouvement (rappels)**
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Description du mouvement d’un système par celui d’un point.<br>
                Position. Trajectoire d’un point.<br>
            </td>
            <td>
                Décrire le mouvement d’un système par celui d’un point et caractériser cette modélisation en termes de perte d’informations.<br>
                Caractériser différentes trajectoires.<br><br>
                <strong>Capacité numérique :</strong> représenter les positions successives d’un système modélisé par un point lors d’une évolution unidimensionnelle ou bidimensionnelle à l’aide d’un langage de programmation.
            </td>
        </tr>
    </tbody>
</table>
</center>


**Vitesse**
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Vecteur déplacement d’un point. Vecteur vitesse moyenne d’un point.<br>
                Vecteur vitesse d’un point.<br>
                Mouvement rectiligne.<br>
            </td>
            <td>
                Définir le vecteur vitesse moyenne d’un point.<br>
                Approcher le vecteur vitesse d’un point à l’aide du vecteur déplacement MM', où M et M’ sont les positions successives à des instants voisins séparés de Δt ; le représenter.<br>
                Caractériser un mouvement rectiligne uniforme ou non uniforme.<br>
                Réaliser et/ou exploiter une vidéo ou une chronophotographie d’un système en mouvement et représenter des vecteurs vitesse ; décrire la variation du vecteur vitesse.<br><br>

                <strong>Capacité numérique :</strong> représenter des vecteurs vitesse d’un système modélisé par un point lors d’un mouvement à l’aide d’un langage de programmation.<br>

                <strong>Capacité mathématique :</strong> représenter des vecteurs.<br>
                Utiliser des grandeurs algébriques.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

<br>

### Préparation de solution et dosage
- La concentration massique
- La solubilité
- La dissolution
- La dilution


<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <strong>Les solutions aqueuses, un exemple de mélange.</strong><br>
                Solvant, soluté.<br>
                Concentration en masse, concentration maximale d’un soluté.<br>
            </td>
            <td>
                Identifier le soluté et le solvant à partir de la composition ou du mode opératoire de préparation d’une solution. Distinguer la masse volumique d’un échantillon et la concentration en masse d’un soluté au sein d’une solution.<br>
                Déterminer la valeur de la concentration en masse d’un soluté à partir du mode opératoire de préparation d’une solution par dissolution ou par dilution.<br>
                Mesurer des masses pour étudier la variabilité du volume mesuré par une pièce de verrerie ; choisir et utiliser la verrerie adaptée pour préparer une solution par dissolution ou par dilution.<br>
            </td>
        </tr>
          <tr>
            <td>
                Du macroscopique au microscopique, de l’espèce chimique à l’entité.<br>
                Espèces moléculaires, espèces ioniques, électroneutralité de la
                matière au niveau macroscopique.<br>
            </td>
            <td>
                Définir une espèce chimique comme une collection d’un nombre très élevé d’entités identiques.<br>
                Exploiter l’électroneutralité de la matière pour associer des espèces ioniques et citer des formules de composés ioniques.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>



<br>



### Mesure et incertitudes
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Variabilité de la mesure d’une grandeur physique.<br>
                <br><br><br><br>

                Incertitude-type.<br>
                Écriture du résultat.<br>
                Valeur de référence.<br>
            </td>
            <td>
                Exploiter une série de mesures indépendantes d’une grandeur physique :
                histogramme, moyenne et écart-type.<br>
                Discuter de l’influence de l’instrument de mesure et du protocole.<br>
                Évaluer qualitativement la dispersion d’une série de mesures indépendantes.<br><br>
                <strong>Capacité numérique :</strong> Représenter l’histogramme associé à une série de mesures à l’aide d’un tableur.<br>
                Expliquer qualitativement la signification d’une incertitude-type et l’évaluer par une approche statistique.<br>
                Écrire, avec un nombre adapté de chiffres significatifs, le résultat d’une mesure.<br>
                Comparer qualitativement un résultat à une valeur de référence.
            </td>
        </tr>
    </tbody>
</table>
</center>


<br>




### Quantité de matière
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <strong>Compter les entités dans un échantillon de matière.</strong> Nombre d’entités dans un échantillon.<br>
                Définition de la mole.<br>
                Quantité de matière dans un échantillon.<br>
<br>
            </td>
            <td>
                Déterminer la masse d’une entité à partir de sa formule brute et de la masse des atomes qui la composent.<br>
                Déterminer le nombre d’entités et la quantité de matière (en mol) d’une espèce dans une masse d’échantillon.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

<br>


### Dosages
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Dosage par étalonnage.<br>
            </td>
            <td>
                Déterminer la valeur d’une concentration en masse et d’une concentration maximale à partir de résultats expérimentaux.<br>
                Déterminer la valeur d’une concentration en masse à l’aide d’une gamme d’étalonnage (échelle de teinte ou mesure de masse volumique).<br>
                <strong>Capacité mathématique :</strong> utiliser une grandeur quotient pour déterminer le numérateur ou le dénominateur.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

<br>



### Forces et principe d’inertie
#### Modéliser une action sur un système
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Modélisation d’une action par une force.<br>
            </td>
            <td>
                Modéliser l’action d’un système extérieur sur le système étudié par une force. Représenter une force par un vecteur ayant une norme, une direction, un sens.
                <br>
            </td>
        </tr>
        <tr>
            <td>
                Principe des actions réciproques (troisième loi de Newton).<br>
            </td>
            <td>
               Exploiter le principe des actions réciproques.<br>
            </td>
        </tr>
        <tr>
            <td>
                Caractéristiques d’une force.<br>
                Exemples de forces :<br>
                <li>force d’interaction gravitationnelle ;</li>
                <li>poids ;</li>
                <li>force exercée par un support et par un fil.</li>
            </td>
            <td>
                Distinguer actions à distance et actions de contact.<br>
                Identifier les actions modélisées par des forces dont les expressions mathématiques sont connues a priori. Utiliser l’expression vectorielle de la force d’interaction gravitationnelle.<br>
                Utiliser l’expression vectorielle du poids d’un objet, approché par la force d’interaction gravitationnelle s’exerçant sur cet objet à la surface d’une planète. Représenter qualitativement la force modélisant l’action d’un support dans des cas simples relevant de la statique.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

#### Principe d'inertie
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Modèle du point matériel.<br>
                Principe d’inertie.<br>
                Cas de situations d’immobilité et de mouvements rectilignes uniformes.<br>
                Cas de la chute libre à une dimension.<br>
            </td>
            <td>
                Exploiter le principe d’inertie ou sa contraposée pour en déduire des informations soit sur la nature du mouvement d’un système modélisé par un point matériel, soit sur les forces.<br>
                Relier la variation entre deux instants voisins du vecteur vitesse d’un système modélisé par un point matériel à l’existence d’actions extérieures modélisées par des forces dont la somme est non nulle, en particulier dans le cas d’un mouvement de chute libre à une dimension (avec ou sans vitesse initiale).<br>
           
            </td>
        </tr>
    </tbody>
</table>
</center>

<br>

### La transformation chimique
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Variabilité de la mesure d’une grandeur physique.<br>
            </td>
            <td>
                Exploiter 
                <strong>Capacité numérique :</strong> Représenter l’histogramme associé à une série de mesures à l’aide d’un tableur.<br>
                Expliquer qualitativement la signification d’une incertitude-type et l’évaluer par une approche statistique.<br>
            </td>
        </tr>
        <tr>
            <td>
                Identification d’espèces chimiques dans un échantillon de matière par des mesures physiques ou des tests chimiques.<br>
            </td>
            <td>
                Identifier, à partir de valeurs de référence, une espèce chimique par ses températures de changement d’état, sa masse volumique ou par des tests chimiques.<br>
                Citer des tests chimiques courants de présence d’eau, de dihydrogène, de dioxygène, de dioxyde de carbone.<br>
                Citer la valeur de la masse volumique de l’eau liquide et la comparer à celles d’autres corps purs et mélanges. Distinguer un mélange d’un corps pur à partir de données expérimentales.<br>
                Mesurer une température de changement d’état, déterminer la masse volumique d’un échantillon, réaliser une chromatographie sur couche mince, mettre en œuvre des tests chimiques, pour identifier une espèce chimique et, le cas échéant, qualifier l’échantillon de mélange.<br>
            </td>
        </tr>
        <tr>
            <td>
                Composition massique d’un mélange.<br>
                Composition volumique de l’air.<br>
            </td>
            <td>
                Citer la composition approchée de l’air et l’ordre de grandeur de la valeur de sa masse volumique.<br>
                Établir la composition d’un échantillon à partir de données expérimentales.<br>
                Mesurer des volumes et des masses pour estimer la composition de mélanges.<br>
                <strong>Capacité mathématique :</strong> utiliser les pourcentages et les fractions.<br>
            </td>
        </tr>


    </tbody>
</table>
</center>

<br>

### Les ondes sonores
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Émission et propagation d’un signal sonore.<br>
            </td>
            <td>
                Décrire le principe de l’émission d’un signal sonore par la mise en vibration d’un objet et l’intérêt de la présence d’une caisse de résonance.<br>
                Expliquer le rôle joué par le milieu matériel dans le phénomène de propagation d’un signal sonore.<br>
            </td>
        </tr>
        <tr>
            <td>
                Vitesse de propagation d’un signal sonore.<br>
            </td>
            <td>
                Citer une valeur approchée de la vitesse de propagation d’un signal sonore dans l’air et la comparer à d’autres valeurs de vitesses couramment rencontrées.<br>
                Mesurer la vitesse d’un signal sonore.<br>
            </td>
        </tr>
        <tr>
            <td>
                Signal sonore périodique, fréquence et période. Relation entre période et fréquence.<br>
            </td>
            <td>
                Définir et déterminer la période et la fréquence d’un signal sonore notamment à partir de sa représentation temporelle. Utiliser une chaîne de mesure pour obtenir des informations sur les vibrations d’un objet émettant un signal sonore.<br>
                Mesurer la période d’un signal sonore périodique. Utiliser un dispositif comportant un microcontrôleur pour produire un signal sonore.<br>
                <strong>Capacité mathématique :</strong> identifier une fonction périodique et déterminer sa période.<br>
            </td>
        </tr>
        <tr>
            <td>
                Perception du son : lien entre fréquence et hauteur ; lien entre forme du signal et timbre ; lien qualitatif entre amplitude, intensité sonore et niveau d’intensité sonore. Échelle de niveaux d’intensité sonore.<br>
            </td>
            <td>
                Citer les domaines de fréquences des sons audibles, des infrasons et des ultrasons.<br>
                Relier qualitativement la fréquence à la hauteur d’un son audible.<br>
                Relier qualitativement intensité sonore et niveau d’intensité sonore.<br>
                Exploiter une échelle de niveau d’intensité sonore et citer les dangers inhérents à l’exposition sonore.<br>
                Enregistrer et caractériser un son (hauteur, timbre, niveau d’intensité sonore, etc.) à l’aide d’un dispositif expérimental dédié, d’un smartphone, etc.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

<br>

### Introduction à l’optique
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Lois de Snell-Descartes pour la réflexion et la réfraction. Indice optique d’un milieu matériel.<br>
            </td>
            <td>
                Exploiter les lois de Snell-Descartes pour la réflexion et la réfraction.<br>
                Tester les lois de Snell-Descartes à partir d’une série de mesures et déterminer l’indice de réfraction d’un milieu.<br>
            </td>
        </tr>
        <tr>
            <td>
                Dispersion de la lumière blanche par un prisme ou un réseau.<br>
            </td>
            <td>
                Décrire et expliquer qualitativement le phénomène de dispersion de la lumière par un prisme.<br>
            </td>
        </tr>
        <tr>
            <td>
                Lentilles, modèle de la lentille mince convergente : foyers, distance focale.<br>
                Image réelle d’un objet réel à travers une lentille mince convergente.<br>
                Grandissement.<br>
                L’œil, modèle de l’œil réduit.<br>
            </td>
            <td>
                Caractériser les foyers d’une lentille mince convergente à l’aide du modèle du rayon lumineux.<br>
                Utiliser le modèle du rayon lumineux pour déterminer graphiquement la position, la taille et le sens de l’image réelle d’un objet plan réel donnée par une lentille mince convergente.<br>
                Définir et déterminer géométriquement un grandissement.<br>
                Modéliser l’œil.<br>
                Produire et caractériser l’image réelle d’un objet plan réel formée par une lentille mince convergente.<br>
                <strong>Capacité mathématique :</strong> utiliser le théorème de Thalès.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>


<br>

### Électricité
<center>
<table>
    <thead>
        <tr>
            <td><center><strong>Notions et contenus</strong></center></td>
            <td><center><strong>Capacités exigibles</strong></center></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Loi des nœuds. Loi des mailles.<br>
            </td>
            <td>
                Exploiter la loi des mailles et la loi des nœuds dans un circuit électrique comportant au plus deux mailles.<br>
                Mesurer une tension et une intensité.<br>
            </td>
        </tr>
        <tr>
            <td>
                Caractéristique tension-courant d’un dipôle.<br>
                Résistance et systèmes à comportement de type ohmique.<br>
                Loi d’Ohm.<br>
            </td>
            <td>
                Exploiter la caractéristique d’un dipôle électrique : point de fonctionnement, modélisation par une relation U = f(I) ou I = g(U).<br>
                Utiliser la loi d’Ohm.<br>
                Représenter et exploiter la caractéristique d’un dipôle.<br>
                
                <strong>Capacité numérique :</strong> représenter un nuage de points associé à la caractéristique d’un dipôle et modéliser la caractéristique de ce dipôle à l’aide d’un langage de programmation.<br>
                <strong>Capacité mathématique :</strong> identifier une situation de proportionnalité.
            </td>
        </tr>
        <tr>
            <td>
                Capteurs électriques.<br>
            </td>
            <td>
                Citer des exemples de capteurs présents dans les objets de la vie quotidienne.<br>
                Mesurer une grandeur physique à l’aide d’un capteur électrique résistif.<br>
                Produire et utiliser une courbe d’étalonnage reliant la résistance d’un système avec une grandeur d’intérêt (température, pression, intensité lumineuse, etc.).<br>
                Utiliser un dispositif avec microcontrôleur et capteur.<br>
            </td>
        </tr>
    </tbody>
</table>
</center>

<br>
