---
title : "Exercices"
correctionvisible : True
---

<div class="lien_pdf_sujet">
  <a  href="../pdf/exercices/2_PC_Chapitre1_DM2.pdf">Sujet.pdf </a>
  

  <a href="../pdf/exercices/Correction/2_PC_Chapitre1_DM2_correction.pdf" class="correction">Correction.pdf</a>
</div>

<br>

# Exercices : Composés ioniques



- **Exercice 1 :** 17 <a href="../images/exercices/manuel/p68.webp">p68</a>
- **Exercice 2 :** 18 <a href="../images/exercices/manuel/p68.webp">p68</a>
- **Exercice 3 :** 21 <a href="../images/exercices/manuel/p68.webp">p68</a>
- **Exercice 4 :** 22 <a href="../images/exercices/manuel/p68.webp">p68</a>
- **Exercice 5 :** 24 <a href="../images/exercices/manuel/p68.webp">p68</a>
- **Exercice 6 :** 25 <a href="../images/exercices/manuel/p68.webp">p68</a>
- **Exercice 7 :** 27 <a href="../images/exercices/manuel/p69.webp">p69</a>






<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

