---
title : "Cours"
---

<div class="lien_pdf_sujet">
  <a  href="../pdf/cours/2_PC_Chapitre1_Cours2.pdf">Cours.pdf </a>
</div>


<center>
    <img src="../pdf/cours/2_PC_Chapitre1_Cours2_p1.webp" width="1200" />
    <img src="../pdf/cours/2_PC_Chapitre1_Cours2_p2.webp" width="1200" />
</center>


??? note "Liens vidéo"
    - [Paul Olivier_Comment déterminer la Formule d'un composé ionique ?](https://www.youtube.com/watch?v=cU7B3W8dFYA)
    - [Paul Olivier_Comment trouver le nom d'un composé ionique ?](https://www.youtube.com/watch?v=GO-lZjKREHw)
    - [Paul Olivier_solide ionique: quels ions le compose ?](https://www.youtube.com/watch?v=td0NKoPPb44)


    - [Les Bons profs _ Molécules,atomes et ions](https://www.youtube.com/watch?v=E6yvElfgcqk)