---
title : "Cours"
---


<div class="lien_pdf_sujet">
  <a  href="../pdf/cours/2_PC_Chapitre1_Cours.pdf">Cours.pdf </a>
</div>




<center>
    <img src="../pdf/cours/2_PC_Chapitre1_Cours_p1.webp" width="1200" />
    <img src="../pdf/cours/2_PC_Chapitre1_Cours_p2.webp" width="1200" />
</center>


??? note "Liens vidéo"
    - [Paul Olivier_Charge des ions](https://www.youtube.com/watch?v=3okQp-SHmaw)
    - [Paul Olivier_Les ions mono et polyatomiques](https://www.youtube.com/watch?v=GMRINarIR_o)
    - [Paul Olivier_Quiz sur les ions](https://www.youtube.com/watch?v=uzeT2nR075I)
    - [Les Bons profs _ Molécules,atomes et ions](https://www.youtube.com/watch?v=E6yvElfgcqk)