﻿---
title : "Activité 1"
correctionvisible : true
---

<div class="lien_pdf_sujet">
  <a  href="../pdf/activites/2_PC_Chapitre1_Activite1.pdf">Sujet.pdf </a>
  

  <a href="../pdf/activites/Corrections/2_PC_Chapitre 1_Activite1_Correction.pdf" class="correction">Correction.pdf</a>
</div>


# Activité 1 : Les entités chimiques


<br>

### **Document 1** : Des espèces chimiques dans le corps humain


<figure>
  <img src="../images/activite/activite_1.jpg" width="900" />
</figure>


<br>

### **Document 2** : Quelques définitions

- *Entité chimique (n.f.)* : collection d’un nombre très élevé d’entités chimiques identiques.
- *Atome (n.m.)* : plus petite particule électriquement neutre qui identifie un élément chimique.
- *Molécule (n.f.)* : entité chimique électriquement neutre, constituée de deux atomes au moins.
- *Ion (n.m.)* : entité chimique porteuse d’une charge électrique.
- *Anion (n.m.)* : ion de charge négative.
- *Cation (n.m.)* : ion de charge positive.

<br>

### **Questions:**

!!! faq ""
    1. Indiquer pour chaque espèce chimique du corps humain l’entité chimique qui lui correspond.
    2. Indiquer la formule des cations présents dans le corps humain.
    3. Indiquer la formule des anions présents dans le corps humain.
    4. Le saccharose de formule brute C~12~H~22~O~11~ est un glucide que nous consommons couramment. Quelle est son entité chimique ? Quelles informations nous apporte la formule brute ?



<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

