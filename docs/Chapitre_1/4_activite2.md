---
title : "Activité 2"
correctionvisible : False
---

<div class="lien_pdf_sujet">
  <a  href="../pdf/activites/2_PC_Chapitre1_Activite2.pdf">Sujet.pdf </a>
  

  <a href="../pdf/activites/Corrections/2_PC_Chapitre1_Activite2_correction.pdf" class="correction">Correction.pdf</a>
</div>


# Activité 2 : Les composés ioniques



<center>
    <img src="../pdf/activites/2_PC_Chapitre1_Activite2_p1.webp" width="1200" />
    <img src="../pdf/activites/2_PC_Chapitre1_Activite2_p2.webp" width="1200" />
</center>


<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

