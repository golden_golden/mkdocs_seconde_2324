---
title : "Exercices"
correctionvisible : true
---

<div class="lien_pdf_sujet">
  <a  href="../pdf/exercices/2_PC_Chapitre1_DM2.pdf">Sujet.pdf </a>
  

  <a href="../pdf/exercices/Correction/2_PC_Chapitre1_DM2_correction.pdf" class="correction">Correction.pdf</a>
</div>

<br>

# Exercices : Isotopes



- **Exercice 1 :** a faire <a href="../images/exercices/manuel/p68.webp">p68</a>

- **Exercice 2 :** a faire <a href="../images/exercices/manuel/p68.webp">p68</a>





<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

