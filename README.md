# Physique Chimie Seconde

Cours de Physique chimie
- **Niveau** : Seconde
- **Année scolaire** : 2023-2024
- **Établissement** : Lycée Bossuet *(Condom)*


## Où voir le site ?
[C'est ici](https://golden_golden.gitlab.io/mkdocs_seconde_2324/)



## Techno
Site réalisé avec :  *mkdocs*

Merci, aux enseignents du [forum de NSI](https://mooc-forums.inria.fr/) pour la découverte de cet outil, et notament à :
- Franck CHAMBON (Francky) pour ses dépots [gitlab](https://gitlab.com/ens-fr)
- Vincent BOUILLO pour ses dépots [gitlab](https://gitlab.com/bouillotvincent) et [ici](https://gitlab.com/ferney-nsi)


## Project status
En cours de développement.
 

## Authors
**Initial**  : VILLEMUR Arnaud

**Contributeurs** : …

*Ajouter votre nom si vous avez contribué !*



## License
Copyright © 2023 VILLEMUR Arnaud <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>.
