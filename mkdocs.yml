site_name: Physique Chimie Seconde
site_description: Cours de physique chimie pour le niveau seconde.
site_url: https://golden_golden.gitlab.io/mkdocs_seconde_2324/
site_author: Arnaud VILLEMUR
copyright: <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Copyright © 2023 Arnaud VILLEMUR</a>.
docs_dir: docs


nav:
    #- ... | Chapitre_16/*
    #- ... | Chapitre_15/*
    #- ... | Chapitre_14/*
    #- ... | Chapitre_13/* 
    #- ... | Chapitre_12/*
    #- ... | Chapitre_11/*
    #- ... | Chapitre_10/*
    #- ... | Chapitre_9/* 
    #- ... | Chapitre_8/*
    #- ... | Chapitre_7/*
    #- ... | Chapitre_6/*
    #- ... | Chapitre_5/*
    #- ... | Chapitre_4/*
    #- ... | Aides maths/*
    #- ... | Fiches methodes/*
    #- ... | Outils numeriques/*

    #- Chapitre 3:
    #    - Activité 1: ./Chapitre_3/10_activite.md
    #    - Activité 2: ./Chapitre_3/10_activite.md
    #    - Activité 3: ./Chapitre_3/10_activite.md
    #    - Activité 4: ./Chapitre_3/10_activite.md
    #    - Cours: ./Chapitre_3/20_cours_modele_atome.md
    #    - Exercices: ./Chapitre_3/25_exercices.md

    - Chapitre 2:
        - Activité 1: ./Chapitre_2/10_activite1.md
        - Exercice: ./Chapitre_2/15_activite2.md
        - Cours: ./Chapitre_2/20_cours_modele_atome.md
        - Exercices: ./Chapitre_2/25_exercices.md
        - Activité 3: ./Chapitre_2/28_activite3.md
        - Cours: ./Chapitre_2/30_cours_element_chimique.md
        - Exercices: ./Chapitre_2/35_exercices.md

    - Chapitre 1:
        - Activité 1: ./Chapitre_1/1_activite1.md
        - Cours: ./Chapitre_1/2_cours_Entites_chimiques.md
        - Exercices: ./Chapitre_1/3_exercices.md
        - Activité 2: ./Chapitre_1/4_activite2.md
        - Cours: ./Chapitre_1/5_cours_composes_ioniques.md
        - Exercices: ./Chapitre_1/6_exercices.md
    

    - Documents:
        - Le programme scolaire: ./Documents/1_programme.md
        - Progression et répartition du programme: ./Documents/2_progression.md
        - Compéthences de la démarche scientifiques: ./Documents/3_compethences_demarche_scientifique.md





theme:
    name: 'material'                # utilisation du thème mkdocs material design, https://squidfunk.github.io/mkdocs-material/
    language: fr                    # Français
    logo: 'assets_extra/images/Logo.png' # Changer le logo du site par celui dans le dossier  assets_extra
    font: false                     # Ne pas utiliser les font google
    palette:                        # Réglages de la colorimétrie générale du site
        scheme: default
    features:
        - toc.integrate             # Perment de fusionner la table des matière de la page dans le sommaire
        - header.autohide           # Permet de suprimer le logo et le lien git dans le header
    highlightjs: true


markdown_extensions:
    - meta                          # Permet d'utiliser des méthadonnées dans les entêtes des fichiers .md                      
    - attr_list                     # Ajout d'attribut html et css, https://squidfunk.github.io/mkdocs-material/reference/images/#images
    - abbr                          # Légender au survol de la sourie des abréviations. https://squidfunk.github.io/mkdocs-material/reference/abbreviations/?h=abbr
    - codehilite                    # Coloration syntaxique de bloc de code
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.details              # Blocs admonition qui peuvent se plier/déplier.
    - pymdownx.superfences          # Imbrication de blocs.
    - pymdownx.tabbed               # Volets glissants.  === "Mon volet"
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight            # Coloration syntaxique du code
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>
    - pymdownx.smartsymbols
    - pymdownx.keys:                # Modélise des touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.critic:              # Permet de commenter le code markdown sans que les remarques n'apparaissent sur le site
        mode: reject
    - pymdownx.betterem:            # Lien avec les espaces surnuméraire après et avant les symboles de mise en forme tel que *
        smart_enable: all
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        clickable_checkbox: true    #   avec cases d'origine
        custom_checkbox: true       #   et cliquables.
    - pymdownx.emoji:               # Émojis  :boom:
        emoji_index:     !!python/name:materialx.emoji.twemoji
        emoji_generator: !!python/name:materialx.emoji.to_svg
    - pymdownx.arithmatex:          # Pour la gestion des formules mathématique en KaTex 
        generic: true
    - toc:                          # lien url vers des les titres et sous titre des pages.
        permalink: ⚓︎
        toc_depth: 3
    




plugins:               # Les plugins doivent être installés pour être fonctionnels ! Cf requierement ou Dockerfile de l'image.
    - macros
    - kroki            # Permet d'utiliser touts les outils de korki https://kroki.io/ et https://pypi.org/project/mkdocs-kroki-plugin/
    - awesome-pages    # Permet d'organiser les fichiers .md dans des sous dossiers de docs.
    #- search          # Affiche une barre de recherche dans le header


extra_javascript:
  - assets_extra/javascripts/config.js   # Utiliser pour la mise en forme des expression mathématique en latex.
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js






extra_css:
  - assets_extra/CSS/extra.css
  - assets_extra/CSS/print.css
  - assets_extra/CSS/qcm.css
